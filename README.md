# API for SGregister

I tillegg til SGregister som en nettside for søk etter foretak med sentral godkjenning har vi også etablert et REST-API som kan brukes til å raskt slå opp og hente inn data fra registeret. Målgruppen for dette APIet er datasystemer og nettsider som ønsker å sjekke om et foretak har sentral godkjenning.

## Dokumentasjon
Dette stedet oppbevarer en [OpenAPI-spesifikasjon av APIet](https://bitbucket.org/dibk/sgregister-api/raw/master/openapi.json). Oppdatert dokumentasjon finnes på [SGregister.dibk.no](https://sgregister.dibk.no/apidocs/index.html). I tillegg er også APIet registrert i [Felles Datakatalog](https://data.norge.no) ([Direkte lenke](https://data.norge.no/dataservices/c443ab25-ae23-3b67-b6b3-3782c08f4d46))